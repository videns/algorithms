#Script de scraping para búsqueda de trabajo en la plataforma EmpleosTI
#Actualmente la plataforma cambió de nombre a hireline.io
#--------------------------------------------------------------------------
#Scraping script for job search on the EmpleosTI platform
#Currently the platform has changed its name to hireline.io.

from asyncio.windows_events import NULL
from distutils.filelist import findall
import pandas as pd;
from bs4 import BeautifulSoup;
from os import listdir
from os.path import isfile, join

baseUrl = 'https://empleosti.com.mx/empleos/a/'
baseDir = 'C:/projects/EmpleosTI/alljobs/'

jobTitlePositions = []
salarys = []
places = []
jobTypes = []
englishLevels = []
enterpriseNames = []
vacancyDescriptions = []
urls = []

def extractData(url):
    print(url)
    soup = NULL
    with open(url, encoding="utf-8") as fp:
        soup = BeautifulSoup(fp, 'html.parser')

    if len(soup.find_all('h4', class_='expTitle-2')) > 0:
        print("Vacante no encontrada")
        return

    if len(soup.find_all('div', class_ = 'internal-alert-eti')) > 0:
        print("Vacante no encontrada")
        return

    #job title position
    jobTitlePosition = soup.find_all('h1', class_='section-title')[0]
    #print(jobTitlePosition.text)
    jobTitlePositions.append(jobTitlePosition.text)

    #Obtains info from vacancy
    allJobInfo = soup.find_all('div', class_='card-panel-eti')[3]

    #Salary
    _salary = allJobInfo.find_all('li')[0]
    salary = _salary.find_all('p', class_ = 'gray-subarea-title')[0]
    if salary.text == '':
        salarys.append('0.0')
    else:
        salarys.append(salary.text)
        #print (salary.text)

    #Place 
    _place = allJobInfo.find_all('li')[1] 
    place = _place.find_all('a')[0]
    #print(place.text)
    places.append(place.text)

    #Job type
    _jobType = allJobInfo.find_all('li')[2] 
    jobType = _jobType.find_all('p', class_ = 'gray-subarea-title')[0]
    #print(jobType.text)
    jobTypes.append(jobType.text)

    #English level
    _e_level  = allJobInfo.find_all('li')[3] 
    e_level = _e_level.find_all('p', class_ = 'gray-subarea-title')[0]
    #print(e_level.text)
    englishLevels.append(e_level.text)

    #Enterprise name
    enterpriseName = allJobInfo.find_all('h3', id ='searchable-enterprise')[0]
    #print(enterpriseName.text)
    enterpriseNames.append(enterpriseName.text)

    #Vacancy description
    vacancyDesc = allJobInfo.find_all('div', id ='vacancy-description')[0]
    #print(vacancyDesc.text)
    vacancyDescriptions.append(vacancyDesc.text)

    #vacancyUrl = baseUrl + url.split(sep = '/')[len(url.split(sep = '/'))]
    #print(vacancyUrl)
    urls.append(url)

def Program():
    for f in [f for f in listdir('C:/projects/EmpleosTI/alljobs/') if isfile(join('C:/projects/EmpleosTI/alljobs/', f))]:
        extractData(baseDir + f)
        #print(f)

    df = pd.DataFrame({'Position': jobTitlePositions, 'Salary': salarys, 'Place' : places, 'Job Type' : jobTypes , 'English Level' : englishLevels, 'Enterprice Name' : enterpriseNames , 'Description' : vacancyDescriptions, 'Url': urls}) 
    df.to_csv('jobs.csv', index=False, encoding='utf-8')

Program()

